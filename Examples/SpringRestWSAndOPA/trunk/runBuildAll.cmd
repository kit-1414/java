set APP_NAME=SingleSignRestWS-2.0-SNAPSHOT
set OPAPP_USER=OnePageAppUser-2.0-SNAPSHOT
set OPAPP_ADMIN=OnePageAppAdmin-2.0-SNAPSHOT


set WEBAPP_DIR=%CATALINA_HOME%\webapps
set WORK_DIR=%BB_SRC_ROOT%\java\Examples\SpringRestWSAndOPA\trunk

rmdir /S /Q %WEBAPP_DIR%\%APP_NAME%
del /Q %WEBAPP_DIR%\%APP_NAME%.war


rmdir /S /Q %WEBAPP_DIR%\%OPAPP_USER%
del /Q %WEBAPP_DIR%\%OPAPP_USER%.war

rmdir /S /Q %WEBAPP_DIR%\%OPAPP_ADMIN%
del /Q %WEBAPP_DIR%\%OPAPP_ADMIN%.war


cd %WORK_DIR%\buildAll
call mvn clean install

copy %WORK_DIR%\SingleSignOnRestWs\target\%APP_NAME%.war  %WEBAPP_DIR%
copy %WORK_DIR%\OnePageAppUser\target\%OPAPP_USER%.war    %WEBAPP_DIR%
copy %WORK_DIR%\OnePageAppAdmin\target\%OPAPP_ADMIN%.war  %WEBAPP_DIR%