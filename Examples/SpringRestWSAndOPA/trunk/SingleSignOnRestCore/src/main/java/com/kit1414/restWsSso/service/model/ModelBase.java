package com.kit1414.restWsSso.service.model;

import java.util.Date;

public class ModelBase {
	private Long id;
    private Date createdOn = new Date();

	public ModelBase(Long id) {
		super();
		this.id = id;
	}

	public ModelBase() {
		super();
	}

	public final Long getId() {
		return id;
	}

	public final void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModelBase other = (ModelBase) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + intString()+"]";
	}
	
	public String intString() {
		return 
				" id=" + getId() +
				" createdOn=" + getCreatedOn();
	}
	
}
