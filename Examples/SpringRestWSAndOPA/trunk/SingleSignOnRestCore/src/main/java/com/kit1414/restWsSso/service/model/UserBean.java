package com.kit1414.restWsSso.service.model;

import java.util.Arrays;

public class UserBean extends ModelBase {
	private String login;
	private String passShadow;
	private String[] roles;

	public UserBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserBean(long id, String login, String passMd5, String[] roles) {
		super(id);
		this.login = login;
		this.passShadow = passMd5;
		this.roles = roles;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassShadow() {
		return passShadow;
	}
	public void setPassShadow(String passShadow) {
		this.passShadow = passShadow;
	}
	public String[] getRoles() {
		return roles;
	}
	public void setRoles(String[] roles) {
		this.roles = roles;
	}
	
	public String intString() {
		return 
			super.intString() +  
				", login=" + login +
				", passShadow=" + passShadow + 
				", roles=" + Arrays.toString(roles);
	}
}
