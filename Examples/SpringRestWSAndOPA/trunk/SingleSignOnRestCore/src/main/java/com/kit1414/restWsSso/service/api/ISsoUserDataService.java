package com.kit1414.restWsSso.service.api;

import java.util.List;

import com.kit1414.restWsSso.service.model.AuthorizeUserRequest;
import com.kit1414.restWsSso.service.model.LoginBean;
import com.kit1414.restWsSso.service.model.UserData;

public interface ISsoUserDataService {
	public static final String URL_PREFIX = ISsoRestPrefix.URL_PREFIX;
	
	public static final String URL_USER_SAVE   = URL_PREFIX + "/userData/save";
	public static final String URL_USER_REMOVE = URL_PREFIX + "/userData/remove/" + ISsoRestPrefix.URL_PARAM_ID;
	public static final String URL_USER_ALL    = URL_PREFIX + "/userData/list";
	public static final String URL_USER_GET    = URL_PREFIX + "/userData/get/" + ISsoRestPrefix.URL_PARAM_ID;
	public static final String URL_USER_CLEAR  = URL_PREFIX + "/userData/clear";
	
	public Long saveUserData(UserData userData);
	public UserData removeUserData(long id);
	public UserData findUserDataById(long id);
	public List<UserData> findAllUserData();
	public void clearAllUserData();
	
	public LoginBean authorizeUser(AuthorizeUserRequest authRequest);

}
