package com.kit1414.restWsSso.service.model;

public class AdminData  extends ModelBase {
	private String adminData;
	private String name;
	private Long parameter;

	public String getAdminData() {
		return adminData;
	}
	public void setAdminData(String adminData) {
		this.adminData = adminData;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getParameter() {
		return parameter;
	}
	public void setParameter(Long parameter) {
		this.parameter = parameter;
	}

	public String intString() {
		return 
				super.intString() + 
				", adminData=" + adminData +
				", name=" + name +
				", parameter=" + parameter;
				
	}
}
