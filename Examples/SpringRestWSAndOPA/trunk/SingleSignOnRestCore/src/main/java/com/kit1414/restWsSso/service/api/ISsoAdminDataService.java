package com.kit1414.restWsSso.service.api;

import java.util.List;

import com.kit1414.restWsSso.service.model.AdminData;
import com.kit1414.restWsSso.service.model.AuthorizeUserRequest;
import com.kit1414.restWsSso.service.model.LoginBean;

public interface ISsoAdminDataService {
	public static final String URL_PREFIX = ISsoRestPrefix.URL_PREFIX;
	
	public static final String URL_ADMIN_SAVE   = URL_PREFIX + "/adminData/save";
	public static final String URL_ADMIN_REMOVE = URL_PREFIX + "/adminData/remove/" + ISsoRestPrefix.URL_PARAM_ID;
	public static final String URL_ADMIN_ALL    = URL_PREFIX + "/adminData/list";
	public static final String URL_ADMIN_GET    = URL_PREFIX + "/adminData/get/" + ISsoRestPrefix.URL_PARAM_ID;
	public static final String URL_ADMIN_CLEAR  = URL_PREFIX + "/adminData/clear";
	
	public Long saveAdminData(AdminData adminData);
	public AdminData removeAdminData(long id);
	public AdminData findAdminDataById(long id);
	public List<AdminData> findAllAdminData();
	public void clearAllAdminData();
	
	public LoginBean authorizeUser(AuthorizeUserRequest authRequest);
}
