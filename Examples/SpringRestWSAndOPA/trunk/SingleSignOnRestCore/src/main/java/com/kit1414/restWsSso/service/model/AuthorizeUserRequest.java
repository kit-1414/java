package com.kit1414.restWsSso.service.model;

public class AuthorizeUserRequest {
	private String login;
	private String password;

	public AuthorizeUserRequest() {
		super();
	}
	public AuthorizeUserRequest(String l, String p) {
		super();
		this.login = l;
		this.password = p;
	}
	
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getLogin() {
		return login;
	}
	@Override
	public String toString() {
		return getClass().getSimpleName() + 
				"[" + 
				"  login=" + login + 
				", password=" + password + 
				"]";
	}

}
