package com.kit1414.restWsSso.service.model;

import java.util.Arrays;

public class LoginBean extends ModelBase {
	private String login;
	private String authToken;
	private String[] roles;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getAuthToken() {
		return authToken;
	}
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	public String[] getRoles() {
		return roles;
	}
	public void setRoles(String[] roles) {
		this.roles = roles;
	}
	public boolean isValidUser() {
		return authToken!= null;
	}
	@Override
	public String intString() {
		return 
				 super.intString() + 
				 ", login=" + login +
				 ", authToken=" + authToken +
		         ", roles="+Arrays.toString(roles);
	}
}
