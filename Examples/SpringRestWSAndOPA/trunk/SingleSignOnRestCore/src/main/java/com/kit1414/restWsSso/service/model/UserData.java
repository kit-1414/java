package com.kit1414.restWsSso.service.model;


public class UserData extends ModelBase {
	private String userName;
	private String description;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String intString() {
		return super.intString() + 
				", userName=" + userName + 
				", description=" + description; 
				
	}

}
