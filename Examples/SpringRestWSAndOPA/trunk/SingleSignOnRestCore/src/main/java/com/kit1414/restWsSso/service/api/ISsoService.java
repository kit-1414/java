package com.kit1414.restWsSso.service.api;

public interface ISsoService extends  ISsoUserDataService, ISsoAdminDataService {
	public static final String URL_LOGIN   = ISsoRestPrefix.URL_PREFIX + "/login";
}
