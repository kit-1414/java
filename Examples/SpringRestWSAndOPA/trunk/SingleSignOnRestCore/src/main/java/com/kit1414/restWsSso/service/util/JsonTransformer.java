package com.kit1414.restWsSso.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonTransformer {
	private static final Logger logger = LoggerFactory.getLogger(JsonTransformer.class);

	private final ObjectMapper mapper = new ObjectMapper();
	
	public String writeObjectToJson(Object obj) {
		if (obj == null) {
			return null;
		}
		try {
			return mapper.writeValueAsString(obj);
		} catch (Throwable ex) {
			logger.error("Conversion to Json fails : " + obj);
		}
		return null;
	}
	public <T> T readObject(String jsonText, Class<T> clazz) {
		T result = null;
		try {
			result = (T)mapper.readValue(jsonText, clazz);
		} catch (Throwable ex) {
			logger.error("JSON read failed : class="+clazz+", json="+jsonText,ex);
			result = null;
		}
		return result;
	}
	
}
