var serviceUrlStart="http://localhost:8080/SingleSignRestWS-2.0-SNAPSHOT/ssoRest";

var app = angular.module('adminApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute'
]);

app.config(function ($routeProvider, $httpProvider) {
    // $httpProvider.defaults.headers.common['Authorization'] = 'Basic YWRtaW5fbG9naW46YmE0NmUxYmIwOWZjMThiOWFmODlkYTNmNDc2Y2RhZjc=';

    $routeProvider.when('/list', {
        templateUrl: 'views/list.html',
        controller: 'ListCtrl'
    }).when('/create', {
        templateUrl: 'views/create.html',
        controller: 'CreateCtrl'
    }).when('/edit/:id', {
        templateUrl: 'views/edit.html',
        controller: 'EditCtrl'
    }).when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
    }).when('/logout', {
        templateUrl: 'views/logout.html',
        controller: 'LogoutCtrl'
    }).otherwise({
        redirectTo: '/list'
    })
});

app.controller('ListCtrl',['$scope', '$http', '$location', '$route', 'ServerService', 'SecureService',
   function ($scope, $http, $location, $route, ServerService, SecureService) {
        // var user = SecureService.getCurrentUser();
        // alert('user='+user);

        ServerService.doGet('/adminData/list',function (data) { $scope.admins = data;});

        $scope.editAdmin = function (admin) {
            console.log(admin);
            $location.path('/edit/'+admin.id);
        }
        $scope.logout = function () {
           SecureService.logout();
           $location.path('/login');
        }

        $scope.deleteAdmin = function (admin) {
            console.log(admin);
            ServerService.doGet('/adminData/remove/'+admin.id,null,null,function() {$route.reload();});
        }
}]);

app.controller('CreateCtrl', [ '$scope', '$http', '$location', 'ServerService', function ($scope, $http, $location, ServerService) {
    $scope.createAdmin = function () {
        console.log($scope.admin);
        ServerService.doPost('/adminData/save',$scope.admin, function (data) {$location.path('/list');});
    }
}]);
app.controller('EditCtrl',['$scope', '$http', '$location','$routeParams', 'ServerService', function ($scope, $http, $location,$routeParams, ServerService) {

    ServerService.doGet('/adminData/get/' + $routeParams.id, function (data) { $scope.admin = data;});
    $scope.updateAdmin = function () {
        console.log($scope.admin);
        ServerService.doPost('/adminData/save',$scope.admin, function (data) {$location.path('/list');});
    }

}]);
app.controller('LoginCtrl',['$scope', '$http', '$location','$routeParams', 'SecureService',
   function ($scope, $http, $location,$routeParams, SecureService) {
        $scope.login = function () {
           SecureService.login($scope.user, function() {
              $location.path('/list');
           });
        }

   }]);
app.controller('LogoutCtrl',['$scope', '$http', '$location','$routeParams', 'ServerService', function ($scope, $http, $location,$routeParams, ServerService) {
        $scope.login = function () {
           alert('logout');
        }
}]);

app.controller('MenuCtrl',['$scope','$rootScope', 'SecureService', function ($scope, $rootScope, SecureService) {
    $scope.isUserBean  = false;
    $scope.isAdminRole = false;
    var loginMsg = SecureService.getLoginNotificationMsg();
    var logountMsg = SecureService.getLogoutNotificationMsg();

    $rootScope.$on(loginMsg, function(event, data) {
       $scope.isUserBean  = true;
       $scope.isAdminRole = true;
       console.log('msg received='+loginMsg+', event='+event+', data='+data);
    });
    $rootScope.$on(logountMsg, function(event, data) {
       $scope.isUserBean  = false;
       $scope.isAdminRole = false;
       console.log('msg received='+logountMsg+', event='+event+', data='+data);
    });
}]);


app.factory('ServerService',['$http',function($http)  {
  var doHttp = function (url, methodName,data, successFunc, errorFunc, alwaysFunc) {
     var targetUrl =  serviceUrlStart + url;
     var request = { method: methodName, url: targetUrl, data: data};
     $http(request).then(function successCallback(response) {
         if (successFunc) {
             successFunc(response.data);
         }
         if (alwaysFunc) {
            alwaysFunc(response);
         }
     }, function errorCallback(response,status) {
         console.log('Error ' + data + ', status='+status)
         if (errorFunc) {
             errorFunc(response.data,status);
         }
         if (alwaysFunc) {
            alwaysFunc(response,status);
         }
     });
  }
  var doGet = function(url, sucessFunc, errorFunc,alwaysFunc ) {
      doHttp(url,'GET', null, sucessFunc, errorFunc,alwaysFunc );
  }
  var doPost = function(url, request, sucessFunc, errorFunc,alwaysFunc) {
      doHttp(url,'POST', request, sucessFunc, errorFunc,alwaysFunc );
  }
  return {
     doGet  : doGet,
     doPost : doPost
  }
}]);
app.factory('SecureService',['$rootScope', '$http','ServerService', 
  function($rootScope, $http, ServerService)  {
      var userBean = null;

      var securedControllers=['ListCtrl','CreateCtrl','EditCtrl','LogoutCtrl'];
      var validRoles=['ROLE_ADMIN'];

      var loginNotificationMsg = 'LOGIN_SUCCESS';
      var logoutNotificationMsg = 'LOGOUT_SUCCESS';

      var raiseEventFunction = function(eventName) {
          $rootScope.$broadcast(eventName);
      };

      var checkValidRole = function(userBean) {
         if (userBean && userBean.roles.length > 0) {
            var intersect = userBean.roles.filter(function(n) {
               return validRoles.indexOf(n) != -1
            });	    
            return intersect.length > 0;
         }
         return false;
      }
      var loginFunction = function(userToLogin, loginSuccessAction) {
          var requestBean = {
             login : userToLogin.login,
             password : userToLogin.password
          }

          var loginSuccess = function(data) {             
              userBean = data;
              if (userBean.authToken && userBean.authToken.length > 0 ) { 
                 if (checkValidRole(userBean)) {
                    setAuthHeader(userBean.authToken);
                    if (loginSuccessAction) {
                       loginSuccessAction();
                    }
                    raiseEventFunction(loginNotificationMsg);
                 } else {
                    alert('user has not access here. He must be admin');
                 }
              } else {
                alert('user not found or wrong password');
              }
          };
          ServerService.doPost('/login', requestBean,loginSuccess,null);
      };
      var setAuthHeader = function(token) {
         var header = null;
         if (token && token.length > 0) {             
            header = 'Basic '+ token;
         }
         $http.defaults.headers.common.Authorization = header; 

      }

      var logoutFunction = function() {
              userBean = null;
              setAuthHeader(null);
              raiseEventFunction(logoutNotificationMsg);
      };
      var checkLoginRedirectFunc = function(nextControllerName) {
         var ctlIndex = securedControllers.indexOf(nextControllerName);
         var securedController = ctlIndex != -1;
         return userBean == null && securedController;
      }
      var servicePtr = {
          getCurrentUser     : function() { return userBean;},
          userBean           : userBean,
          checkLoginRedirect : checkLoginRedirectFunc,
          login              : loginFunction,
          logout             : logoutFunction,
          getLoginNotificationMsg  : function() {return loginNotificationMsg; },
          getLogoutNotificationMsg : function() {return logoutNotificationMsg; }
      };
      return servicePtr;
}]);

app.run(['$rootScope', '$location', 'SecureService', function ($rootScope, $location, SecureService) {
    $rootScope.$on('$routeChangeStart', function (event, next, current) {
        console.log('On start  : templateUrl=' + next.templateUrl +', controller=' + next.controller);
        var nextControllerName = next.controller;
        var user = SecureService.getCurrentUser();
        var needLogin = SecureService.checkLoginRedirect(next.controller);
        if (needLogin) {
            $location.path('/login');
        }
    });
}]);
app.directive('usersList', function() {
    return {
        restrict: 'E',
        templateUrl: 'views/usersListDirective.html',
        replace: true
    };
});