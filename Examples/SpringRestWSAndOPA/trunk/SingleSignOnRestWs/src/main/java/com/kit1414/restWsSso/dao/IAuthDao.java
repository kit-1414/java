package com.kit1414.restWsSso.dao;

import com.kit1414.restWsSso.service.model.LoginBean;
import com.kit1414.restWsSso.service.model.UserBean;

public interface IAuthDao extends IDaoBase<UserBean, Long> {
	public LoginBean authorizeUser(String login, String password);
	public LoginBean authorizeLoggedUser(String login, String passwordShadow);
	
	public UserBean addUser(String login, String password, String[] roles );
}
