package com.kit1414.restWsSso.dao.impl;

import com.kit1414.restWsSso.dao.IPkGeneratorLong;

public class PkGeneratorLongImpl implements IPkGeneratorLong {
	private long pkCounter;
	public PkGeneratorLongImpl(long startValue) {
		pkCounter = startValue;
	}
	public PkGeneratorLongImpl() {
		this(0);
	}
	@Override
	public Long getNextId() {
		return ++pkCounter;
	}
	
}
