package com.kit1414.restWsSso.dao;

import com.kit1414.restWsSso.service.model.UserData;

public interface IUserDataDao extends IDaoBase<UserData, Long> {

}
