package com.kit1414.restWsSso.dao;

import java.io.Serializable;

public interface IPkGenerator<PK extends Serializable> {
	public PK getNextId();
}
