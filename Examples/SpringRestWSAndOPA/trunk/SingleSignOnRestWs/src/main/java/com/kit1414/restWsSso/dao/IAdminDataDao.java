package com.kit1414.restWsSso.dao;

import com.kit1414.restWsSso.service.model.AdminData;

public interface IAdminDataDao extends IDaoBase<AdminData, Long> {

}

