package com.kit1414.restWsSso.controller;

import java.util.ArrayList;
import java.util.List;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;

import com.kit1414.restWsSso.dao.IAuthDao;
import com.kit1414.restWsSso.service.model.LoginBean;

public class MyAuthenticationProvider implements AuthenticationProvider {
	private static final Logger logger = LoggerFactory.getLogger(MyAuthenticationProvider.class);
	
	private IAuthDao authDao;
 
    @Override
    public Authentication authenticate(Authentication authentication) 
      throws AuthenticationException {
        String name = authentication.getName();
        String passwordShadow = authentication.getCredentials().toString();
        logger.debug("authenticate() l/shad=" + name+"/"+passwordShadow);
        
        LoginBean lb = authDao.authorizeLoggedUser(name, passwordShadow);
        if (lb.isValidUser()) {
            logger.debug("authenticate() user found : " + lb);
            List<GrantedAuthority> grantedAuths = new ArrayList<>();
            for (String r : lb.getRoles()) {
            	grantedAuths.add(new MyGrantedAuthority(r));
            }
            return new UsernamePasswordAuthenticationToken(name, passwordShadow, grantedAuths);
        	
        } else {
            logger.debug("authenticate() NOBODY found : " + lb);
            throw new MyAuthExcepion("Unable to auth against third party systems");
        }
    }
    @SuppressWarnings("serial")
	public static class MyAuthExcepion extends  AuthenticationException {
		public MyAuthExcepion(String msg) {
			super(msg);
			
		}
    }

	@SuppressWarnings("serial")
	public static class MyGrantedAuthority implements GrantedAuthority {
		private final String authority;

		public MyGrantedAuthority(String authority) {
			super();
			this.authority = authority;
		}

		@Override
		public String getAuthority() {
			return authority;
		}
	}
 
    @Override
    public boolean supports(Class<?> authentication) {
        logger.debug("supports() authentication=" + authentication);
        boolean result= authentication.equals(UsernamePasswordAuthenticationToken.class);
        logger.debug("supports() result=" + result);
        return result;
    }

	public IAuthDao getAuthDao() {
		return authDao;
	}

	public void setAuthDao(IAuthDao authDao) {
		this.authDao = authDao;
	}

}
