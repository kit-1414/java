package com.kit1414.restWsSso.dao.impl;

import com.kit1414.restWsSso.dao.IAdminDataDao;
import com.kit1414.restWsSso.dao.IPkGeneratorLong;
import com.kit1414.restWsSso.service.model.AdminData;

public class AdminDataDaoInMemoryImpl  extends DaoInMemoryBaseImpl<AdminData>  implements IAdminDataDao {

	public AdminDataDaoInMemoryImpl(IPkGeneratorLong pkLong) {
		super(pkLong);
	}

	@Override
	protected AdminData cloneEntity(AdminData v) {
		if (v == null) { 
			return null;
		}
		AdminData copy = new AdminData();
		copy.setId(v.getId());
		copy.setAdminData(v.getAdminData());
		copy.setName(v.getName());
		copy.setParameter(v.getParameter());
		return copy;
	}




}
