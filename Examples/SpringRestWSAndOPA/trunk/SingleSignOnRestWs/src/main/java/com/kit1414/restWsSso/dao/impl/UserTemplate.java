package com.kit1414.restWsSso.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;

public class UserTemplate {
	private String login;
	private String password;
	private String[] roles;
	
	public UserTemplate(String l, String p, String[] r) {
		super();
		login= l;
		password = p;
		roles = r;
	}
	public UserTemplate(String lineToParse) {
		super();
		String [] parsed = lineToParse.split("\\,");
		if (parsed.length > 0) {
			this.login = parsed[0].trim();
		}
		if (parsed.length > 1) {
			this.password = parsed[1].trim();
		}
		if (parsed.length > 2) {
			ArrayList<String> roles = new ArrayList<>(10);
			for (int i=2; i<parsed.length; i++) {
				if (parsed[i]!= null) {
					roles.add(parsed[i].trim());
				}
			}
			this.roles = roles.toArray(new String[roles.size()]);
		}
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String[] getRoles() {
		return roles;
	}
	public void setRoles(String[] roles) {
		this.roles = roles;
	}
	public boolean isOk() {
		return login!= null && login.length() > 0 && password!= null && password.length() >0;
	}
	@Override
	public String toString() {
		return 
				getClass().getSimpleName() + 
				"[" + 
				"  login=" + login + 
				", password=" + password + 
				", roles=" + Arrays.toString(roles) +
				"]";
	}
	

}
