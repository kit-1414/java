package com.kit1414.restWsSso.dao.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.util.DigestUtils;

import com.kit1414.restWsSso.dao.IAuthDao;
import com.kit1414.restWsSso.dao.IPkGeneratorLong;
import com.kit1414.restWsSso.service.model.LoginBean;
import com.kit1414.restWsSso.service.model.UserBean;

public class AuthDaoInMemoryImpl extends DaoInMemoryBaseImpl<UserBean>  implements IAuthDao  {

	protected AuthDaoInMemoryImpl(IPkGeneratorLong g, String[] initUsers) {
		super(g);
		if (initUsers!= null) {
			for (String u : initUsers) {
				UserTemplate ut = new UserTemplate(u);
				if (ut.isOk()) {
					addUser(ut);
				} else {
					throw new RuntimeException("Wrong user template format : string="+u+", ");
				}
				
			}
		}
	}

	@Override
	public LoginBean authorizeUser(String login, String password) {
		return authorizeUserByShadowPass(login, md5(password));		
	}
	@Override
	public LoginBean authorizeLoggedUser(String login, String passwordShadow) {
		return authorizeUserByShadowPass(login, passwordShadow);		
	}
	private LoginBean authorizeUserByShadowPass(String login, String passwordShadow) {
		LoginBean lb = new LoginBean();
		lb .setLogin(login);
		List<UserBean> list = this.findAll();
		for (UserBean user : list ) {
			if (user.getLogin().equalsIgnoreCase(login)) {
				if (user.getPassShadow().equals(passwordShadow)) {
					lb.setId(user.getId());
					lb.setCreatedOn(user.getCreatedOn());
					lb.setRoles(copyStrArray(user.getRoles()));
					String authToken = login+":"+passwordShadow;
					byte[] encodedAuth = Base64.encodeBase64(authToken.getBytes() );
					lb.setAuthToken(new String( encodedAuth ));
				}
				break;
			}
		}
		return lb;		
	}
	
	
	@Override
	public synchronized UserBean addUser(String login, String password, String[] roles) {
		return addUser(new UserTemplate(login, password, roles));
	}
	private UserBean addUser(UserTemplate ut) {
		UserBean ub = new UserBean();
		ub.setId(getNextId());
		ub.setCreatedOn(new Date());
		ub.setLogin(ut.getLogin());
		ub.setPassShadow(md5(ut.getPassword()));
		ub.setRoles(copyStrArray(ut.getRoles()));
		super.save(ub);
		return cloneEntity(ub);
		
	}

	@Override
	protected UserBean cloneEntity(UserBean inVal) {
		if (inVal==null) {
			return null;
		}
		UserBean ub = new UserBean(inVal.getId(), inVal.getLogin(), inVal.getPassShadow(),copyStrArray(inVal.getRoles()));
		ub .setCreatedOn(inVal.getCreatedOn());
		return ub;
	}
	
	@Override
	public synchronized Long save(UserBean entity) {
		throw new RuntimeException("Deprecated method called");
	}
	
	private static String md5(String pass) {
		return pass==null ? "" : DigestUtils.md5DigestAsHex(pass.getBytes()); 
	}
	private String[] copyStrArray(String[] array) {
		if (array==null) {
			return null;
		}
		return Arrays.copyOf(array, array.length);
	}
}
