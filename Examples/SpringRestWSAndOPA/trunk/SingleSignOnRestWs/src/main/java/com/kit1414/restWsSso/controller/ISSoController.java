package com.kit1414.restWsSso.controller;

public interface ISSoController {

	public static final String URL_USER_SAVE   = "/userData/save";
	public static final String URL_USER_REMOVE = "/userData/remove/{id}";
	public static final String URL_USER_ALL = "/userData/list";
	public static final String URL_USER_GET = "/userData/get/{id}";
	
	
}
