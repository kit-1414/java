package com.kit1414.restWsSso.dao.impl;


import org.testng.annotations.Test;

import com.kit1414.restWsSso.dao.IAuthDao;
import com.kit1414.restWsSso.service.model.UserBean;

public class UserTemplateTest {
	@Test
	public void constructorTest() {
		String userProto ="login_1  ,   pass_2 , role_3, role_4  ";
		UserTemplate ut = new UserTemplate(userProto);
		info(ut);
		
	}

	
	@Test
	public void authDaoTest() {
		String[] users = {
				"user1,pass1",
				"user2,pass2, role2_1",
				"user3,pass3, role3_1, role3_2",
		};
		IAuthDao authDao = new AuthDaoInMemoryImpl(new PkGeneratorLongImpl(1000), users);
		for (UserBean ub : authDao.findAll()) {
			info(ub);
		}
	}
	
	public static void info(Object msg) {
		System.out.println("INFO: " + msg);
	}

}
