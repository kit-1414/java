package com.kit1414.restWsSso.service.client;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.DigestUtils;
import org.springframework.web.client.RestTemplate;

import com.kit1414.restWsSso.service.util.JsonTransformer;

public class BaseRestClient {
	private static final Logger logger = LoggerFactory.getLogger(BaseRestClient.class);
	
	public static final String HEADER_AUTH_NAME="Authorization";
	public static final String HEADER_AUTH_START="Basic ";

	private final String serverUrl;
	private final String login;
	private final String password;
	
	public BaseRestClient(String url) {
		this(url,null,null);
	}
	public BaseRestClient(String srvUrl, String login, String pass) {
		
		this.serverUrl = srvUrl;
		this.login = login;
		this.password = pass;
	}
	
	protected HttpHeaders createHeaders(){
		HttpHeaders hh = new HttpHeaders();
		if (login!= null && password!= null) {
			String passShadow = DigestUtils.md5DigestAsHex(password.getBytes());
			String auth = login + ":" + passShadow;
			byte[] encodedAuth = Base64.encodeBase64(auth.getBytes() );
			String authHeader = HEADER_AUTH_START + new String( encodedAuth );
			
			logger.debug("createHeaders() : authHeader=" + authHeader);
			
			hh.add(HEADER_AUTH_NAME, authHeader);
		}
		return hh;
	}
	protected HttpEntity<?> createEntityForGet(){
		return new HttpEntity<>(createHeaders());
	}
	protected HttpEntity<?> createEntityForPost(Object obj) {
		JsonTransformer jt = new JsonTransformer();
		HttpHeaders hh   = createHeaders();
		hh.setContentType(MediaType.APPLICATION_JSON);
		return new HttpEntity<>(jt.writeObjectToJson(obj),hh);
	}
	protected <T> T exchangeLocalPost(String url, Class<T> responseType, Object variable) {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<?> he = createEntityForPost(variable);
		
		HttpEntity<T> response = restTemplate.exchange(serverUrl+url, HttpMethod.POST, he, responseType);
		return response.getBody();
	}
	protected <T> T exchangeLocalGet(String url, Class<T> responseType) {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<T> response = restTemplate.exchange(serverUrl+url, HttpMethod.GET, createEntityForGet(), responseType);
		return response.getBody();
	}
	protected static String replaceTempate(String text,String template, Object obj) {
		int pos = text.indexOf(template);
		String result = text;
		if (pos!= -1) {
			String start = text.substring(0, pos);
			String end = text.substring(pos + template.length());
			result =  start + obj + end;
		}
		return result;
	}


}
