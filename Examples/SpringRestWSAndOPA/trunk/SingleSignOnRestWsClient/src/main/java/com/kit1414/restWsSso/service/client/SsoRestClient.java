package com.kit1414.restWsSso.service.client;

import java.util.Arrays;
import java.util.List;

import com.kit1414.restWsSso.service.api.ISsoRestPrefix;
import com.kit1414.restWsSso.service.api.ISsoService;
import com.kit1414.restWsSso.service.model.AdminData;
import com.kit1414.restWsSso.service.model.AuthorizeUserRequest;
import com.kit1414.restWsSso.service.model.LoginBean;
import com.kit1414.restWsSso.service.model.UserData;

public class SsoRestClient extends BaseRestClient implements ISsoService {
	
	public SsoRestClient(String url, String login, String pass) {
		super(url, login, pass);
	}
	public SsoRestClient(String url) {
		super(url);
	}
	
	@Override
	public Long saveUserData(UserData userData) {
		Long id= exchangeLocalPost(ISsoService.URL_USER_SAVE, Long.class, userData);
		return id;
	}

	@Override
	public UserData removeUserData(long id) {
		String url = replaceTempate(ISsoService.URL_USER_REMOVE,ISsoRestPrefix.URL_PARAM_ID,id);
		UserData ud = exchangeLocalGet(url, UserData.class);
		return ud;
	}

	@Override
	public UserData findUserDataById(long id) {
		String url = replaceTempate(ISsoService.URL_USER_GET,ISsoRestPrefix.URL_PARAM_ID,id);
		UserData ud = exchangeLocalGet(url, UserData.class);
		return ud;
	}

	@Override
	public List<UserData> findAllUserData() {
		UserData[] ud = exchangeLocalGet(ISsoService.URL_USER_ALL, UserData[].class);
		return Arrays.asList(ud);
	}

	@Override
	public void clearAllUserData() {
		exchangeLocalGet(ISsoService.URL_USER_CLEAR, Long.class);
	}

	@Override
	public Long saveAdminData(AdminData adminData) {
		Long id= exchangeLocalPost(ISsoService.URL_ADMIN_SAVE, Long.class,adminData);
		return id;
	}

	@Override
	public AdminData removeAdminData(long id) {
		String url = replaceTempate(ISsoService.URL_ADMIN_REMOVE,ISsoRestPrefix.URL_PARAM_ID,id);
		AdminData ad = exchangeLocalGet(url, AdminData.class);
		return ad;
	}

	@Override
	public AdminData findAdminDataById(long id) {
		String url = replaceTempate(ISsoService.URL_ADMIN_GET,ISsoRestPrefix.URL_PARAM_ID,id);
		AdminData ad = exchangeLocalGet(url, AdminData.class);
		return ad;
	}

	@Override
	public List<AdminData> findAllAdminData() {
		AdminData[] resultArray = exchangeLocalGet(ISsoService.URL_ADMIN_ALL, AdminData[].class);
		return Arrays.asList(resultArray);
	}

	@Override
	public void clearAllAdminData() {
		exchangeLocalGet(ISsoService.URL_ADMIN_CLEAR,Long.class);
	}

	@Override
	public LoginBean authorizeUser(AuthorizeUserRequest authRequest) {
		LoginBean loginBean= exchangeLocalPost(ISsoService.URL_LOGIN, LoginBean.class,authRequest);
		return loginBean;	
	}
}
