package com.kit1414.restWsSso.service.client;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.kit1414.restWsSso.service.api.ISsoUserDataService;
import com.kit1414.restWsSso.service.model.AuthorizeUserRequest;
import com.kit1414.restWsSso.service.model.LoginBean;
import com.kit1414.restWsSso.service.model.UserData;

/**
 * 
 * @author kit_1414
 *
 */
public class SsoUserRestClientTest extends RestClientBase {

	public static final String USER_LOGIN    = "user_login";
	public static final String USER_PASSWORD = "user_login_pass";
	public static final String USER_ROLE = "ROLE_USER";
	
	public static void main(String args[]) {
		new SsoUserRestClientTest().userDataTest();
	}
	// It's integration tests...
	@Test(enabled=false)
	public void userDataTest() {
		final SsoRestClient client = new SsoRestClient(SERVER_URI,USER_LOGIN,USER_PASSWORD);
		final ISsoUserDataService userService = client;
		
		userService.clearAllUserData();
		
		List<UserData> userDataList = userService.findAllUserData();
		Assert.assertEquals(userDataList.size(),0);

		Long id1 = userService.saveUserData(buildUserData(null));
		Long id2 = userService.saveUserData(buildUserData(null));
		long defId = 44;
		Long id3 = userService.saveUserData(buildUserData(defId));
		Assert.assertEquals(id3.longValue(),defId);
		
		
		userDataList = userService.findAllUserData();
		
		info ("3 users added : size=" + userDataList.size());
		Assert.assertEquals(userDataList.size(),3);
		
		UserData ud2=userService.removeUserData(id2);
		info ("2nd user removed : " + ud2);
		Assert.assertNotNull(ud2);
		
		userDataList = userService.findAllUserData();
		info ("2 users added : size=" + userDataList.size());
		Assert.assertEquals(userDataList.size(),2);
		
		UserData ud1=userService.findUserDataById(id1);
		
		String newName = "dkdnakdmaksdmlkas";
		ud1.setUserName(newName);
		
		long ud1_1 = userService.saveUserData(ud1);
		info ("user updated id="+ud1 + ", user data1="+ud1+", saved " + ud1_1);

		
		UserData ud11=userService.findUserDataById(id1);
		info ("user updated cheak="+ud11);
		Assert.assertEquals(ud11.getUserName(),newName);
		
		userService.clearAllUserData();
		userDataList = userService.findAllUserData();
		
		info ("Clear all: size=" + userDataList.size());
		Assert.assertEquals(userDataList.size(),0);
		

		LoginBean lb = userService.authorizeUser(new AuthorizeUserRequest(USER_LOGIN, USER_PASSWORD));
		info("user auth_0 = " + lb);
		Assert.assertEquals(lb.getLogin(), USER_LOGIN);
		Assert.assertEquals(lb.getRoles()[0], USER_ROLE);

		AuthorizeUserRequest ar_1= new AuthorizeUserRequest(USER_LOGIN+"wdwdew", USER_PASSWORD);
		LoginBean lb_1 = userService.authorizeUser(ar_1);
		info("user auth_1 = " + lb_1);
		Assert.assertNull(lb_1.getAuthToken());
		Assert.assertEquals(lb_1.getLogin(),ar_1.getLogin());

		AuthorizeUserRequest ar_2= new AuthorizeUserRequest(USER_LOGIN, USER_PASSWORD+"dqwdqw");
		LoginBean lb_2 = userService.authorizeUser(ar_2);
		info("user auth_2 = " + lb_2);
		Assert.assertNull(lb_2.getAuthToken());
		Assert.assertEquals(lb_2.getLogin(),ar_2.getLogin());

		info("done");
	}

}
