package com.kit1414.restWsSso.service.client;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.kit1414.restWsSso.service.api.ISsoAdminDataService;
import com.kit1414.restWsSso.service.model.AdminData;
import com.kit1414.restWsSso.service.model.AuthorizeUserRequest;
import com.kit1414.restWsSso.service.model.LoginBean;


public class SsoAdminRestClientTest extends RestClientBase {
	public static final String ADMIN_LOGIN = "admin_login";
	public static final String ADMIN_PASSWORD  = "admin_login_pass";
	public static final String ADMIN_ROLE = "ROLE_ADMIN";

	public static void main(String args[]) {
		new SsoAdminRestClientTest().adminDataTest();
	}
	// It's integration tests...
	@Test(enabled=false)
	public void adminDataTest() {
		final SsoRestClient client = new SsoRestClient(SERVER_URI, ADMIN_LOGIN, ADMIN_PASSWORD );
		final ISsoAdminDataService adminService = client;

		adminService.clearAllAdminData();
		List<AdminData> adminDataList = adminService.findAllAdminData();
		Assert.assertEquals(adminDataList.size(),0);


		Long id1 = adminService.saveAdminData(buildAdminData(null));
		Long id2 = adminService.saveAdminData(buildAdminData(null));
		long defId = 45;
		Long id3 = adminService.saveAdminData(buildAdminData(defId));
		Assert.assertEquals(id3.longValue(), defId);

		adminDataList = adminService.findAllAdminData();

		info("3 admins added : size=" + adminDataList.size());

		AdminData ud2 = adminService.removeAdminData(id2);
		info("2nd admin removed : " + ud2);
		Assert.assertNotNull(ud2);


		adminDataList = adminService.findAllAdminData();
		info("2 admins added : size=" + adminDataList.size());
		Assert.assertEquals(adminDataList.size(),2);

		AdminData ad1 = adminService.findAdminDataById(id1);

		String newName = "dkdnakdmaksdmlkas";
		ad1.setAdminData(newName);

		long ad1_1 = adminService.saveAdminData(ad1);
		info("admin updated id=" + ad1 + ", user data1=" + ad1 + ", saved "+ ad1_1);

		AdminData ud11 = adminService.findAdminDataById(id1);
		info("user updated cheak=" + ud11);
		Assert.assertEquals(ud11.getAdminData(), newName);

		adminService.clearAllAdminData();
		adminDataList = adminService.findAllAdminData();

		info("Clear all: size=" + adminDataList.size());
		Assert.assertEquals(adminDataList.size(),0);

		LoginBean lb = adminService.authorizeUser(new AuthorizeUserRequest(ADMIN_LOGIN, ADMIN_PASSWORD));
		info("user auth_0 = " + lb);
		Assert.assertEquals(lb.getLogin(), ADMIN_LOGIN);
		Assert.assertEquals(lb.getRoles()[0], ADMIN_ROLE);

		AuthorizeUserRequest ar_1= new AuthorizeUserRequest(ADMIN_LOGIN+"wdwdew", ADMIN_PASSWORD);
		LoginBean lb_1 = adminService.authorizeUser(ar_1);
		info("user auth_1 = " + lb_1);
		Assert.assertNull(lb_1.getAuthToken());
		Assert.assertEquals(lb_1.getLogin(),ar_1.getLogin());

		AuthorizeUserRequest ar_2= new AuthorizeUserRequest(ADMIN_LOGIN, ADMIN_PASSWORD +"dqwdqw");
		LoginBean lb_2 = adminService.authorizeUser(ar_2);
		info("user auth_2 = " + lb_2);
		Assert.assertNull(lb_2.getAuthToken());
		Assert.assertEquals(lb_2.getLogin(),ar_2.getLogin());

		info("done");
		
	}

}
