var serviceUrlStart="http://localhost:8080/SingleSignRestWS-1.0-SNAPSHOT/ssoRest";
var app = angular.module('adminApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute'
]);

app.config(function ($routeProvider) {
    $routeProvider.when('/list', {
        templateUrl: 'views/list.html',
        controller: 'ListCtrl'
    }).when('/create', {
        templateUrl: 'views/create.html',
        controller: 'CreateCtrl'
    }).when('/edit/:id', {
        templateUrl: 'views/edit.html',
        controller: 'EditCtrl'
    }).otherwise({
        redirectTo: '/list'
    })
});

app.controller('ListCtrl', function ($scope, $http, $location, $route) {
    $http.get(serviceUrlStart + '/adminData/list').success(function (data) {
        $scope.admins = data;
    }).error(function (data, status) {
        console.log('Error ' + data)
    })
    $scope.editAdmin = function (admin) {
        console.log(admin);
        $location.path('/edit/'+admin.id);
    }
    $scope.deleteAdmin = function (admin) {
        console.log(admin);
        $http.get(serviceUrlStart + '/adminData/remove/'+admin.id).success(function (data) {
	    }).error(function (data, status) {
            console.log('Error ' + data)
        })
       	$route.reload();
    }
});

app.controller('CreateCtrl', function ($scope, $http, $location) {
    $scope.createAdmin = function () {
        console.log($scope.admin);
        $http.post(serviceUrlStart + '/adminData/save', $scope.admin).success(function (data) {
            $location.path('/list');
        }).error(function (data, status) {
            console.log('Error ' + data)
        })
    }
});
app.controller('EditCtrl', function ($scope, $http, $location,$routeParams) {
    $http.get(serviceUrlStart + '/adminData/get/' + $routeParams.id).success(function (data) {
        $scope.admin = data;
    }).error(function (data, status) {
        console.log('Error ' + data)
    })
    $scope.updateAdmin = function () {
        console.log($scope.admin);
        $http.post(serviceUrlStart + '/adminData/save', $scope.admin).success(function (data) {
            $location.path('/list');
        }).error(function (data, status) {
            console.log('Error ' + data)
        })
    }
});