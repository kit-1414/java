var serviceUrlStart="http://localhost:8080/SingleSignRestWS-1.0-SNAPSHOT/ssoRest";
var app = angular.module('userapp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute'
]);

app.config(function ($routeProvider) {
    $routeProvider.when('/list', {
        templateUrl: 'views/list.html',
        controller: 'ListCtrl'
    }).when('/create', {
        templateUrl: 'views/create.html',
        controller: 'CreateCtrl'
    }).when('/edit/:id', {
        templateUrl: 'views/edit.html',
        controller: 'EditCtrl'
    }).otherwise({
        redirectTo: '/list'
    })
});

app.controller('ListCtrl', function ($scope, $http, $location, $route) {
    $http.get(serviceUrlStart + '/userData/list').success(function (data) {
        $scope.users = data;
    }).error(function (data, status) {
        console.log('Error ' + data)
    })
    $scope.editUser = function (user) {
        console.log(user);
        $location.path('/edit/'+user.id);
    }
    $scope.deleteUser = function (user) {
        console.log(user);
        $http.get(serviceUrlStart + '/userData/remove/'+user.id).success(function (data) {
	    }).error(function (data, status) {
            console.log('Error ' + data)
        })
       	$route.reload();
    }
});

app.controller('CreateCtrl', function ($scope, $http, $location) {
    $scope.createUser = function () {
        console.log($scope.user);
        $http.post(serviceUrlStart + '/userData/save', $scope.user).success(function (data) {
            $location.path('/list');
        }).error(function (data, status) {
            console.log('Error ' + data)
        })
    }
});
app.controller('EditCtrl', function ($scope, $http, $location,$routeParams) {
    $http.get(serviceUrlStart + '/userData/get/' + $routeParams.id).success(function (data) {
        $scope.user = data;
    }).error(function (data, status) {
        console.log('Error ' + data)
    })
    $scope.updateUser = function () {
        console.log($scope.user);
        $http.post(serviceUrlStart + '/userData/save', $scope.user).success(function (data) {
            $location.path('/list');
        }).error(function (data, status) {
            console.log('Error ' + data)
        })
    }
});