package com.kit1414.restWsSso.service.client;

import java.util.List;

import com.kit1414.restWsSso.service.api.ISsoAdminDataService;
import com.kit1414.restWsSso.service.api.ISsoUserDataService;
import com.kit1414.restWsSso.service.model.AdminData;
import com.kit1414.restWsSso.service.model.UserData;

public class RestClientBase {
	public static final String SERVER_URI = "http://localhost:8080/SingleSignRestWS-1.0-SNAPSHOT";


	protected static boolean testUserData(ISsoUserDataService userSrvClient) {

		{
			userSrvClient.clearAllUserData();
			List<UserData> users = userSrvClient.findAllUserData();
			if (users.size() != 0) {
				info("Error in 8 step ");
				return false;
			}
		}

		final long fId = 44l;

		Long rId = userSrvClient.saveUserData(buildUserData(fId));
		if (!rId.equals(fId)) {
			info("Error in 1 step");
			return false;
		}

		{
			userSrvClient.saveUserData(buildUserData(null));
			List<UserData> users = userSrvClient.findAllUserData();
			if (users.size() != 2) {
				info("Error in 2 step");
				return false;
			}
			for (UserData ud : users) {
				info("step 3 : "+ud);
				if (!checkValid(null, ud)) {
					info("Error in 3 step " + ud);
					return false;
				}

			}
		}
		UserData ud = userSrvClient.findUserDataById(fId);
		if (!checkValid(fId, ud)) {
			info("Error in 4 step " + ud);
			return false;
		}
		String newUsername = "soem new name + 7777";
		ud.setUserName(newUsername);

		userSrvClient.saveUserData(ud);
		ud = userSrvClient.findUserDataById(fId);
		info("step 5 : "+ud);

		if (!checkValid(fId, ud)) {
			info("Error in 5 step " + ud);
			return false;
		}
		if (!newUsername.equals(ud.getUserName())) {
			info("Error in 6 step " + ud);
			return false;
		}
		{
			userSrvClient.removeUserData(fId);
			List<UserData> users = userSrvClient.findAllUserData();
			if (users.size() != 1) {
				info("Error in 7 step ");
				return false;
			}
		}
		{
			userSrvClient.clearAllUserData();
			List<UserData> users = userSrvClient.findAllUserData();
			if (users.size() != 0) {
				info("Error in 8 step ");
				return false;
			}
		}
		info("userData tested OK");
		return true;
	}
	protected static boolean testAdminData(ISsoAdminDataService adminSrvClient) {

		{
			adminSrvClient.clearAllAdminData();
			List<AdminData> admins = adminSrvClient.findAllAdminData();
			if (admins.size() != 0) {
				info("Error in 0 step ");
				return false;
			}
		}

		final long fId = 44l;

		Long rId = adminSrvClient.saveAdminData(buildAdminData(fId));
		if (!rId.equals(fId)) {
			info("Error in 1 step");
			return false;
		}

		{
			adminSrvClient.saveAdminData(buildAdminData(null));
			List<AdminData> admins= adminSrvClient.findAllAdminData();
			if (admins.size() != 2) {
				info("Error in 2 step");
				return false;
			}
			for (AdminData ad : admins) {
				info("step 3 : "+ad);
				if (!checkValid(null, ad)) {
					info("Error in 3 step " + ad);
					return false;
				}

			}
		}
		AdminData ad = adminSrvClient.findAdminDataById(fId);
		if (!checkValid(fId, ad)) {
			info("Error in 4 step " + ad);
			return false;
		}
		String newName = "soem new name + 7777";
		ad.setName(newName);

		adminSrvClient.saveAdminData(ad);
		ad = adminSrvClient.findAdminDataById(fId);
		info("step 5 : "+ad);

		if (!checkValid(fId, ad)) {
			info("Error in 5 step " + ad);
			return false;
		}
		if (!newName.equals(ad.getName())) {
			info("Error in 6 step " + ad);
			return false;
		}
		{
			adminSrvClient.removeAdminData(fId);
			List<AdminData> admins = adminSrvClient.findAllAdminData();
			if (admins.size() != 1) {
				info("Error in 7 step ");
				return false;
			}
		}
		{
			adminSrvClient.clearAllAdminData();
			List<AdminData> admin = adminSrvClient.findAllAdminData();
			if (admin.size() != 0) {
				info("Error in 8 step ");
				return false;
			}
		}
		info("AdinData tested OK");
		return true;
	}
	public static void info(Object obj) {
		System.out.println("INFO : " + obj);

	}

	protected static UserData buildUserData(Long id) {
		UserData ud = new UserData();
		ud.setId(id);
		ud.setUserName("userName-" + id);
		ud.setDescription("user description-" + id);
		return ud;
	}

	protected static boolean checkValid(Long id, UserData ud) {
		if (ud.getId() == null) {
			return false;
		}
		if (ud.getDescription() == null) {
			return false;
		}
		if (ud.getUserName() == null) {
			return false;
		}
		return id == null ? true : id.equals(ud.getId());
	}

	protected static AdminData buildAdminData(Long id) {
		AdminData ad = new AdminData();
		ad.setId(id);
		ad.setName("Name-" + id);
		ad.setAdminData("some adminData-"+id);
		ad.setParameter(2434L);
		return ad;
	}

	protected static boolean checkValid(Long id, AdminData ad) {
		if (ad.getId() == null) {
			return false;
		}
		if (ad.getName() == null) {
			return false;
		}
		if (ad.getAdminData() == null) {
			return false;
		}
		if (ad.getParameter() == null) {
			return false;
		}
		return id == null ? true : id.equals(ad.getId());
	}

}
