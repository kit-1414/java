package com.kit1414.restWsSso.service.client;

import com.kit1414.restWsSso.service.api.ISsoService;
import com.kit1414.restWsSso.service.api.ISsoUserDataService;


public class FillServiceByData extends RestClientBase {

	public static final String SERVER_URI = "http://localhost:8080/SingleSignRestWS-1.0-SNAPSHOT";

	public static void main(String args[]) {
		// Do integration test with Sso REST client
		final ISsoService client = new SsoRestClient(SERVER_URI);
		final ISsoUserDataService userClient = client;
		
		userClient.saveUserData(buildUserData(null));
		userClient.saveUserData(buildUserData(null));
		userClient.saveUserData(buildUserData(null));
		
	}

	public static void info(Object obj) {
		System.out.println("INFO : " + obj);

	}


}
