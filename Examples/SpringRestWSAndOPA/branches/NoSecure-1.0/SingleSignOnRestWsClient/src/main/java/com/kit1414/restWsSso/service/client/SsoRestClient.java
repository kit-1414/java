package com.kit1414.restWsSso.service.client;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.kit1414.restWsSso.service.api.ISsoRestPrefix;
import com.kit1414.restWsSso.service.api.ISsoService;
import com.kit1414.restWsSso.service.model.AdminData;
import com.kit1414.restWsSso.service.model.UserData;

public class SsoRestClient implements ISsoService {

	private final String serverUrl;
	
	public SsoRestClient(String srvUrl) {
		this.serverUrl = srvUrl;
	}
	
	@Override
	public Long saveUserData(UserData userData) {
		RestTemplate restTemplate = new RestTemplate();
		Long id= restTemplate.postForObject(serverUrl+ISsoService.URL_USER_SAVE, userData, Long.class);
		return id;
	}

	@Override
	public UserData removeUserData(long id) {
		RestTemplate restTemplate = new RestTemplate();
		String url = replaceTempate(serverUrl+ISsoService.URL_USER_REMOVE,ISsoRestPrefix.URL_PARAM_ID,id);
		UserData ud = restTemplate.getForObject(url, UserData.class);
		return ud;
	}

	@Override
	public UserData findUserDataById(long id) {
		RestTemplate restTemplate = new RestTemplate();
		String url = replaceTempate(serverUrl+ISsoService.URL_USER_GET,ISsoRestPrefix.URL_PARAM_ID,id);
		UserData ud = restTemplate.getForObject(url, UserData.class);
		return ud;
	}

	@Override
	public List<UserData> findAllUserData() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<UserData[]> responseEntity = restTemplate.getForEntity(serverUrl+ISsoService.URL_USER_ALL, UserData[].class);
		UserData[] resultArray=responseEntity.getBody();
		return Arrays.asList(resultArray);
	}

	@Override
	public void clearAllUserData() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getForObject(serverUrl+ISsoService.URL_USER_CLEAR,Long.class);
	}

	@Override
	public Long saveAdminData(AdminData adminData) {
		RestTemplate restTemplate = new RestTemplate();
		Long id= restTemplate.postForObject(serverUrl+ISsoService.URL_ADMIN_SAVE, adminData, Long.class);
		return id;
	}

	@Override
	public AdminData removeAdminData(long id) {
		RestTemplate restTemplate = new RestTemplate();
		String url = replaceTempate(serverUrl+ISsoService.URL_ADMIN_REMOVE,ISsoRestPrefix.URL_PARAM_ID,id);
		AdminData ad = restTemplate.getForObject(url, AdminData.class);
		return ad;
	}

	@Override
	public AdminData findAdminDataById(long id) {
		RestTemplate restTemplate = new RestTemplate();
		String url = replaceTempate(serverUrl+ISsoService.URL_ADMIN_GET,ISsoRestPrefix.URL_PARAM_ID,id);
		Object rObj = restTemplate.getForObject(url, AdminData.class);
		return (AdminData)rObj;
	}

	@Override
	public List<AdminData> findAllAdminData() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<AdminData[]> responseEntity = restTemplate.getForEntity(serverUrl+ISsoService.URL_ADMIN_ALL, AdminData[].class);
		AdminData[] resultArray=responseEntity.getBody();
		return Arrays.asList(resultArray);
	}

	@Override
	public void clearAllAdminData() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getForObject(serverUrl+ISsoService.URL_ADMIN_CLEAR,Long.class);
		
	}
	private String replaceTempate(String text,String template, Object obj) {
		int pos = text.indexOf(template);
		String result = text;
		if (pos!= -1) {
			String start = text.substring(0, pos);
			String end = text.substring(pos + template.length());
			result =  start + obj + end;
		}
		return result;
	}

}
