package com.kit1414.restWsSso.service.util;

import org.apache.commons.codec.binary.Base64;		

public class Base64Utils {
	
	public static String encode(String text) {
		
		byte[] encodedBytes = Base64.encodeBase64(text.getBytes());
		return  new String(encodedBytes);
	}

	public static String decode(String text) {
		
		byte[] encodedBytes = Base64.decodeBase64(text.getBytes());
		return  new String(encodedBytes);
		
	}
}
