set APP_NAME=SingleSignRestWS-1.0
set OPAPP_USER=OnePageAppUser-1.0
set OPAPP_ADMIN=OnePageAppAdmin-1.0


set WEBAPP_DIR=F:\JavaTools\apache-tomcat-8.0.21\webapps
set WORK_DIR=F:\_Git\_java\_srcRoot\Examples\SpringRestWSAndOPA\branches\NoSecure-1.0

rmdir /S /Q %WEBAPP_DIR%\%APP_NAME%
del /Q %WEBAPP_DIR%\%APP_NAME%.war


rmdir /S /Q %WEBAPP_DIR%\%OPAPP_USER%
del /Q %WEBAPP_DIR%\%OPAPP_USER%.war

rmdir /S /Q %WEBAPP_DIR%\%OPAPP_ADMIN%
del /Q %WEBAPP_DIR%\%OPAPP_ADMIN%.war


cd %WORK_DIR%\buildAll
call mvn clean install

copy %WORK_DIR%\SingleSignOnRestWs\target\%APP_NAME%.war  %WEBAPP_DIR%
copy %WORK_DIR%\OnePageAppUser\target\%OPAPP_USER%.war    %WEBAPP_DIR%
copy %WORK_DIR%\OnePageAppAdmin\target\%OPAPP_ADMIN%.war  %WEBAPP_DIR%