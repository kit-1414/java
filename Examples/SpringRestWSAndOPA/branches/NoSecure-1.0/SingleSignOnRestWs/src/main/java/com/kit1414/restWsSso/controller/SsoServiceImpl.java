package com.kit1414.restWsSso.controller;

import java.util.List;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kit1414.restWsSso.dao.IAdminDataDao;
import com.kit1414.restWsSso.dao.IUserDataDao;
import com.kit1414.restWsSso.service.api.ISsoService;
import com.kit1414.restWsSso.service.model.AdminData;
import com.kit1414.restWsSso.service.model.UserData;

public class SsoServiceImpl implements ISsoService {
	private static final Logger logger = LoggerFactory.getLogger(SsoControllerRest.class);

	private IUserDataDao userDao;
	private IAdminDataDao adminDao;
	
	
	@Override
	public Long saveUserData(UserData ud) {
		logger.debug("saveUserData() >> ud=" + ud);
		Long result = userDao.save(ud); 
		logger.debug("saveUserData() << result=" + result);
		return result;
	}
	@Override
	public UserData removeUserData(long id) {
		logger.debug("removeUserData()  >> id=" + id);
		UserData result = userDao.remove(id); 
		logger.debug("removeUserData() << result=" + result);
		return result;
	}
	@Override
	public UserData findUserDataById(long id) {
		logger.debug("findUserDataById() >> id=" + id);
		UserData result = userDao.findById(id); 
		logger.debug("findUserDataById() << result=" + result);
		return result;
	}
	@Override
	public List<UserData> findAllUserData() {
		logger.debug("findAllUserData() >> ");
		List<UserData> result = userDao.findAll(); 
		logger.debug("findAllUserData() << result.size()=" + result.size());
		return result;
	}
	@Override
	public void clearAllUserData() {
		logger.debug("clearAllUserData() >> ");
		userDao.clearAll(); 
		logger.debug("clearAllUserData() << ");
	}

	@Override
	public Long saveAdminData(AdminData ad) {
		logger.debug("saveAdminData() >> sd=" + ad);
		Long result = adminDao.save(ad); 
		logger.debug("saveAdminData() << result=" + result);
		return result;
	}
	@Override
	public AdminData removeAdminData(long id) {
		logger.debug("removeAdminData()  >> id=" + id);
		AdminData result = adminDao.remove(id); 
		logger.debug("removeAdminData() << result=" + result);
		return result;
	}
	@Override
	public AdminData findAdminDataById(long id) {
		logger.debug("findAdminDataById() >> id=" + id);
		AdminData result = adminDao.findById(id); 
		logger.debug("findAdminDataById() << result=" + result);
		return result;
	}
	@Override
	public List<AdminData> findAllAdminData() {
		logger.debug("findAllAdminData() >> ");
		List<AdminData> result = adminDao.findAll(); 
		logger.debug("findAllAdminData() << result.size()=" + result.size());
		return result;
	}
	@Override
	public void clearAllAdminData() {
		logger.debug("clearAllAdminData() >> ");
		adminDao.clearAll(); 
		logger.debug("clearAllAdminData() << ");
	}

	public IUserDataDao getUserDao() {
		return userDao;
	}

	public void setUserDao(IUserDataDao userDao) {
		this.userDao = userDao;
	}
	public IAdminDataDao getAdminDao() {
		return adminDao;
	}
	public void setAdminDao(IAdminDataDao adminDao) {
		this.adminDao = adminDao;
	}
}
