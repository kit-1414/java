package com.kit1414.restWsSso.dao.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.kit1414.restWsSso.dao.IDaoBase;
import com.kit1414.restWsSso.dao.IPkGeneratorLong;
import com.kit1414.restWsSso.service.model.ModelBase;

public  abstract class DaoInMemoryBaseImpl<E extends ModelBase> implements IDaoBase<E, Long> {
	
	private final Map<Long, E> repo = new LinkedHashMap<>();
	private final IPkGeneratorLong pkGenerator;
	protected abstract E cloneEntity(E inVal);
	
	protected DaoInMemoryBaseImpl(IPkGeneratorLong g) {
		this.pkGenerator = g;
	}
	
	@Override
	public synchronized Long save(E entity) {
		E copy = cloneEntity(entity); 
		if (copy.getId() == null) {
			copy.setId(pkGenerator.getNextId());
			entity.setId(copy.getId());
		}
		repo.put(copy.getId(), copy);
		return entity.getId();
	}

	@Override
	public synchronized E remove(Long id) {
		return repo.remove(id);
	}

	@Override
	public E findById(Long id) {
		E entity = repo.get(id);
		return entity == null ? null : cloneEntity(entity);
	}

	@Override
	public List<E> findAll() {
		ArrayList<E> result = new ArrayList<>(repo.size());
		for (E e : repo.values()) {
			result.add(cloneEntity(e));
		}
		return result;
	}
	@Override
	public void clearAll() {
		repo.clear();
	}
}
