package com.kit1414.restWsSso.dao.impl;

import com.kit1414.restWsSso.dao.IPkGeneratorLong;
import com.kit1414.restWsSso.dao.IUserDataDao;
import com.kit1414.restWsSso.service.model.UserData;

public class UserDataDaoInMemoryImpl extends DaoInMemoryBaseImpl<UserData>  implements IUserDataDao {

	public UserDataDaoInMemoryImpl(IPkGeneratorLong pkLong) {
		super(pkLong);
	}

	@Override
	protected UserData cloneEntity(UserData v) {
		if (v == null) { 
			return null;
		}
		UserData copy = new UserData();
		copy.setId(v.getId());
		copy.setUserName(v.getUserName());
		copy.setDescription(v.getDescription());
		return copy;
	}
}
