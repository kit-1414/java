package com.kit1414.restWsSso.dao;

import java.util.List;

public  interface  IDaoBase <T, T_PK_TYPE> {
	T_PK_TYPE save(T entity); 
	T remove(T_PK_TYPE id);
	T findById(T_PK_TYPE id);
	List<T> findAll();
	void clearAll();
}
