package com.kit1414.restWsSso.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kit1414.restWsSso.service.api.ISsoService;
import com.kit1414.restWsSso.service.model.AdminData;
import com.kit1414.restWsSso.service.model.UserData;
import com.kit1414.restWsSso.service.util.JsonTransformer;

@RestController
public class SsoControllerRest implements ISSoController {
	
	private static final Logger logger = LoggerFactory.getLogger(SsoControllerRest.class);

	private ISsoService ssoService;
	
	
	@RequestMapping(value = ISsoService.URL_USER_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long saveUser(@RequestBody String rBody) {
		logger.debug("saveUser() >> ud=" + rBody);
		UserData ud = getJsonTransformener().readObject(rBody, UserData.class);
		Long result = ssoService.saveUserData(ud); 
		logger.debug("saveUser() << result=" + result);
		return result;
	}
	@RequestMapping(value = ISsoService.URL_USER_REMOVE, method = RequestMethod.GET)
	public @ResponseBody UserData removeUser(@PathVariable("id") long id) {
		logger.debug("removeUser()  >> id=" + id);
		UserData result = ssoService.removeUserData(id); 
		logger.debug("removeUser() << result=" + result);
		return result;
	}
	@RequestMapping(value = ISsoService.URL_USER_GET, method = RequestMethod.GET)
	public @ResponseBody UserData findUserById(@PathVariable("id") long id) {
		logger.debug("findUserById() >> id=" + id);
		UserData result = ssoService.findUserDataById(id); 
		logger.debug("findUserById() << result=" + result);
		return result;
	}
	@RequestMapping(value = ISsoService.URL_USER_ALL, method = RequestMethod.GET)
	public @ResponseBody List<UserData> findAllUserData() {
		logger.debug("findAllUserData() >> ");
		List<UserData> result = ssoService.findAllUserData(); 
		logger.debug("findAllUserData() << result.size()=" + result.size());
		return result;
	}
	@RequestMapping(value = ISsoService.URL_USER_CLEAR, method = RequestMethod.GET)
	public @ResponseBody Long clearAllUserData() {
		logger.debug("clearAllUserData() >> ");
		ssoService.clearAllUserData(); 
		logger.debug("clearAllUserData() << ");
		return 0L;
	}
	
	@RequestMapping(value = ISsoService.URL_ADMIN_SAVE, method = RequestMethod.POST)
	public @ResponseBody Long saveAdminData(@RequestBody String rBody) {
		logger.debug("saveAdminData() >> ud=" + rBody);
		AdminData ad = getJsonTransformener().readObject(rBody, AdminData.class);
		Long result = ssoService.saveAdminData(ad); 
		logger.debug("saveAdminData() << result=" + result);
		return result;
	}
	@RequestMapping(value = ISsoService.URL_ADMIN_REMOVE, method = RequestMethod.GET)
	public @ResponseBody AdminData removeAdminData(@PathVariable("id") long id) {
		logger.debug("removeAdminData()  >> id=" + id);
		AdminData result = ssoService.removeAdminData(id); 
		logger.debug("removeAdminData() << result=" + result);
		return result;
	}
	@RequestMapping(value = ISsoService.URL_ADMIN_GET, method = RequestMethod.GET)
	public @ResponseBody AdminData findAdminDataById(@PathVariable("id") long id) {
		logger.debug("findAdminDataById() >> id=" + id);
		AdminData result = ssoService.findAdminDataById(id); 
		logger.debug("findAdminDataById() << result=" + result);
		return result;
	}
	@RequestMapping(value = ISsoService.URL_ADMIN_ALL, method = RequestMethod.GET)
	public @ResponseBody List<AdminData> findAllAdminData() {
		logger.debug("findAllAdminData() >> ");
		List<AdminData> result = ssoService.findAllAdminData(); 
		logger.debug("findAllAdminData() << result.size()=" + result.size());
		return result;
	}
	@RequestMapping(value = ISsoService.URL_ADMIN_CLEAR, method = RequestMethod.GET)
	public @ResponseBody Long clearAllAdminData() {
		logger.debug("clearAllAdminData() >> ");
		ssoService.clearAllAdminData(); 
		logger.debug("clearAllAdminData() << ");
		return 0L;
	}
	private JsonTransformer getJsonTransformener() {
		return new JsonTransformer();
	}
	public ISsoService getSsoService() {
		return ssoService;
	}
	public void setSsoService(ISsoService ssoService) {
		this.ssoService = ssoService;
	}
}